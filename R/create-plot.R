# Simple script analysing the Australian weather data

library("tidyverse")

# Read in the data

df = read_csv("data/weatherAUS.csv")

# Create a scatter plot of Humidity9am and Temp9am
# a location

city = "Sydney"

scatter_plot = df %>%
  filter(
    Location == city) %>%
  ggplot(
    aes(x = Temp9am, y = Humidity9am)) + 
  geom_point() +
  labs(title = "Humidity and Temperatur for Brisbane") +
  theme_minimal()

# Save the plot in graphics/
ggsave(
  filename = "graphics/scatter-temp-humidity.png",
  plot = scatter_plot
)

# Create a bar chart of average rainfall
bar_chart =
  df %>%
  group_by(Location) %>%
  summarise(average_rainfall = mean(Rainfall, na.rm = TRUE)) %>%
  ggplot(aes(y = Location, x = average_rainfall)) +
  geom_col(fill = "skyblue")

ggsave(filename = "graphics/barchart-avg-rainfall.png", plot = bar_chart)
